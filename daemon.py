import os
import sys
import random
import threading
import logging
import keyboard
import time
import datetime
import traceback
import textwrap

from escpos.printer import Usb

KEY = 'space'
EVENT_START_HOUR = 19
EVENT_START_MINUTE = 45
EVENT_END_HOUR = 23
EVENT_END_MINUTE = 00
EVENT_MIN_DELAY = 45
EVENT_MAX_DELAY = 75
#EVENT_MIN_DELAY = 4
#EVENT_MAX_DELAY = 7

# Set global variables based on the OS
if sys.platform.startswith("linux"):
    DEVICE = "LinuxDevice"
else:
    DEVICE = "WindowsDevice"

# Logging setup
log_format = '%(asctime)s - %(levelname)s - %(message)s'
logging.basicConfig(filename='_app.log', level=logging.DEBUG, format=log_format)
#stdout_handler = logging.StreamHandler(sys.stdout)
#stdout_handler.setFormatter(logging.Formatter(log_format))
logger = logging.getLogger()
#logger.addHandler(stdout_handler)
logger.setLevel(logging.DEBUG)

def printer(lines):
    if len(lines) < 2:
        logger.error("File contains less than 2 lignes! We won't print anything")
        return

    for line in lines:
        logger.info(line)

    try:
        """ Seiko Epson Corp. Receipt Printer (EPSON TM-T88III) """
        p = Usb(0x04b8, 0x0202, 0) #, profile="TM-T88III")
    
        #p.line_spacing()
        p.line_spacing(90)
    
        # Print logo
        p.image("./images/logo.jpg")

        # First line is title
        p.set(align="left", font=0, bold=True, underline=True, double_height=True, double_width=True, flip=False)
        p.text(f"{lines[0]}\n")
    
        # Other lines are content
        p.set(align="left", font=0, bold=True, underline=False, double_height=False, double_width=False, flip=False)
        #p.set(align="left", font=1, bold=True, underline=False, double_height=True, double_width=True, flip=False)   # 28 characters wide
        for line in lines[1:]:
            line = line.replace("\n", " §")
            #print(line)
            lines2 = wrap_text(line, 40)
            for line2 in lines2:
                line2 = line2.replace("§", "")
                print(line2)
                p.text(f"{line2}\n")
        p.cut()
        p.close()
    except:
        logger.error("Error with printer : {}".format(traceback.format_exc()))
        try:
            p.close()
        except:
            pass

def wrap_text(text, columns=28):
    tw = textwrap.TextWrapper(width=columns,break_long_words=False,replace_whitespace=False)
    t = tw.wrap(text)
    return t

def get_data(folder):
    files = [f for f in os.listdir(folder) if f.endswith(".txt")]
    if not files:
        return []
    random_file = random.choice(files)
    with open(os.path.join(folder, random_file), "r") as file:
        return file.readlines()

def key1():
    logger.info("Key 1 pressed")
    data = get_data(folder="./people_analysis")
    printer(data)

def key2():
    logger.info("Key 2 pressed")

def key3():
    logger.info("Key 3 pressed")

def key4():
    logger.info("Key 4 pressed")

def event():
    logger.info("Event")
    data = get_data(folder="./events")
    printer(data)


def event_thread():
    while True:
        try:
            # Get the current time
            current_time = datetime.datetime.now().time()
            
            # Define the start and end times
            start_time = datetime.time(EVENT_START_HOUR, EVENT_START_MINUTE)
            end_time = datetime.time(EVENT_END_HOUR, EVENT_END_MINUTE)
            
            # Check if the current time is between start_time and end_time
            if start_time <= current_time <= end_time:
                logging.info(f"The current time is between {start_time} and {end_time}")
                # Sleep for a random time between EVENT_MIN_DELAY and EVENT_MAX_DELAY minutes
                sleep_time = random.randint(EVENT_MIN_DELAY, EVENT_MAX_DELAY)
                for idx in range(0, sleep_time):
                    next_time = sleep_time - idx
                    logger.info(f"Next event will trigger in {next_time} minutes")
                    time.sleep(60)

                # Trigger the event function
                event()
            else:
                logging.info(f"The current time is NOT between {start_time} and {end_time}")
                time.sleep(60)

        except KeyboardInterrupt:
            logger.info("Event thread interrupted")
            break
        except Exception as e:
            logger.error(f"Error in event thread: {str(e)}")
            time.sleep(60)
            continue

if __name__ == "__main__":
    try:
        logger.info(f"Running on {DEVICE}")

        # Start the event thread
        event_thread = threading.Thread(target=event_thread)
        event_thread.daemon = True
        event_thread.start()

        # Listen for keyboard input
        keyboard.add_hotkey(KEY, key1)
        keyboard.add_hotkey('z', key2)
        keyboard.add_hotkey('e', key3)
        keyboard.add_hotkey('r', key4)

        logger.info("Press '{}', 'z', 'e', or 'r' to trigger functions. Press Ctrl+C to exit.".format(KEY))

        # Keep the main thread running
        while True:
            pass

    except KeyboardInterrupt:
        logger.info("Application terminated by user")
    except Exception as e:
        logger.error(f"Error in main thread: {str(e)}")

